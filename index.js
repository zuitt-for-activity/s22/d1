console.log('Moshi');




const fruits = ['mango','water melon', 'banana', 'strawberry'];
console.log(fruits);

//================== push method ===============
fruits.push('apple');

console.log(fruits);
// add item to the last part of the array




//================== pop method ===============
fruits.pop();

console.log(fruits);
// delete the last part of the array




//================== push method ===============
fruits.unshift('orange');

console.log(fruits);

// adding item to the first part of the array




//================== shift method ===============

fruits.shift();
console.log(fruits);

// delete item in the first part of the array


//================== splice method ===============

fruits.splice(1, 2, 'papaya')
console.log(fruits);
// This method can delete or replace or add item
// it has 3 parameter
// 1 parameter is the index you will going to start.
// 2 parameter is 




//================== sort method ===============
fruits.sort();
console.log(fruits);
// Will sort the array base on the items in the array.


//================== reverse method ===============
fruits.reverse();
console.log(fruits);
// Will sort reverse the array base on the items in the array.







//================== indexOf method ===============

// finding item from the first item in the array 


//================== lastIndexOf method ===============
// finding the item from the last of the array

//================== slice method ===============
let newFruits = fruits.slice(2,4);
console.log(newFruits);

//================== toString method ===============

let stringFruits = fruits.toString();
console.log(stringFruits);

// convert the array items to string with comma in between of the items.






//================== concat method ===============

let tasksArrayA = ["drink HTML", "eat javascript"];
let tasksArrayB = ['inhale CSS', 'breathe SASS'];
let tasksArrayC = [ 'get git', 'be node'];

let task2 = tasksArrayA.concat(tasksArrayC);
console.log(task2);

// adding the array to another array



//================== toString method ===============

let users = ['richard', 'mosh', 'lea', 'borga'];
users.join(' - ');

console.log(users.join(' - '));
// concat the array items and combine into one string



const tools = ['html', 'css', 'javascript', 'react', 'nodejs', 'bootstrap'];

//Iteration Array Methods

//================== foreach method ===============

let thisTools = [];
tools.forEach((item)=>{
    item.length > 4 ? thisTools.push(item) :null;
})

console.log(thisTools);

//================== map method ===============

const newTools = tools.map((item, index)=> `${index + 1}. ${item}`);
console.log(newTools);
// Create new array that give new value



//================== Every method ===============


//================== some method ===============


//================== reduce method ===============

const grade2 = [75, 88, 90, 73, 100, 95, 83, 70];
const totalGrade = grade2.reduce((a,b)=> a + b);
console.log('This is for the reduce method');
console.log(totalGrade);
// return the total for all the item in the array


//================== sort method ===============
const grade1 = [75, 88, 90, 73, 100, 95, 83, 70];
const sortedGrade1 = grade1.sort((a,b)=> b-a );
console.log('sorted items');
console.log(sortedGrade1);



//================== filter method ===============
const grade = [75, 88, 90, 73, 100, 95, 83, 70];
const passGrade = grade.filter(item=> item > 74);
console.log(passGrade);
// return a new array that contains the elements which meets the given condition.




//================== include method ===============
const tools1 = ['html', 'css', 'javascript', 'react', 'nodejs', 'bootstrap'];

const istools1 = tools1.includes('css');
console.log(istools1);
// return boolean value if the item is in the array.


//testing
//testing 2